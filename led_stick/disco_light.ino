#include <Adafruit_NeoPixel.h>

#define PIXEL_PIN    6    // Digital IO pin connected to the NeoPixels.

#define PIXEL_COUNT 113

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_RGB + NEO_KHZ800);

enum modes {
	VUMETER,
	TRIPPEL_PEAKS,
	ALL_LEDS,
	VU_FROM_CENTER,
	STARMAP,
	MODES_LAST,
	FIRE,
};

int VU;
int VU_max;
int _VU = 0;
int cycle_count = 10;

void Glow(int initial, uint32_t color = 0);
void glow_from_center(int vu);

void setup()
{
	Serial.begin(9600); //set baud rate

	strip.begin();
	strip.show(); // Initialize all pixels to 'off'

	VU = analogRead(5);

	randomSeed(VU);

}

#define NUM_COLORS 5

uint32_t colors[] = {
	strip.Color(255, 0, 0),
	strip.Color(0, 0, 255),
	strip.Color(0, 255, 0),
	strip.Color(255, 255, 255),
	strip.Color(255, 128, 0),
	strip.Color(255, 0, 255),
	strip.Color(0xe6, 0x67, 0xff),
	strip.Color(0x1f, 0xff, 0xfb)
	};

uint32_t peak_color;

enum modes mode = VUMETER;

uint8_t number_of_peaks = 0;

int samples[32];
uint8_t samples_index = 0;
int sample_avg = 0;

uint8_t stars[PIXEL_COUNT];

void loop()
{
	VU = analogRead(5); //set VU as value of pin A5 reading

	VU = VU - 512;

	VU = abs(VU)*1.2;

	if (VU_max < 5) {
		peak_color = 0;
	}

	if (!--cycle_count) {

		if (mode == 1) {
			cycle_count = 3;
			VU_max -=1;
		} else {
			VU_max--;
			cycle_count = 5;
		}
	}

	static int filtered_vu = 0;
	//filtered_vu = filtered_vu/2 + VU/2;
	//filtered_vu = VU;

	sample_avg -= samples[samples_index]/16;
	sample_avg += VU/16;
	samples[samples_index] = VU;
	samples_index++;
	samples_index %= 32;

	filtered_vu = sample_avg;

		if (VU_max < (sample_avg - 3)) {
			VU_max = sample_avg;

			peak_color = colors[random(sizeof(colors)/4)];

			stars[random(PIXEL_COUNT)] = 255;

			if (number_of_peaks++ > 50) {
				mode = (enum modes)random((uint8_t)MODES_LAST);
				number_of_peaks = 0;
			}
		}
	//mode = STARMAP;

	//Serial.write((uint8_t)filtered_vu);

// Main visulaizer

	switch (mode) {
		case VUMETER:
			Glow(filtered_vu);
			PeakVisualizer(VU_max, peak_color);
				//Glow(PIXEL_COUNT);
			break;
		case TRIPPEL_PEAKS:
			Glow(0);
			PeakVisualizer(VU_max, peak_color);
			PeakVisualizer(VU_max*2/3, peak_color);
			PeakVisualizer(VU_max/3, peak_color);
			break;
		case ALL_LEDS:
			Glow(111, peak_color);
			break;
		case VU_FROM_CENTER:
			glow_from_center(filtered_vu);
			break;
		case STARMAP:
			StarMap();
			break;
		case FIRE:
			FireEffect();
			break;
		default:
			Glow(0);
	}

	strip.show();
}

void Glow(int initial, uint32_t color)//function to glow LEDs
{
	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (i < initial) {
			if (color == 0) {
				strip.setPixelColor(i, Wheel(i));
			} else {
				strip.setPixelColor(i, color);
			}
		} else {
			strip.setPixelColor(i, 0);
		}

	}
}

void glow_from_center(int vu)
{
	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (i < (56-vu/2) || i > (56+vu/2)) {
			strip.setPixelColor(i, 0);
		}	else {
			strip.setPixelColor(i,Wheel(abs(i-56)));
		}
	}
}

void PeakVisualizer(int pos,uint32_t peak_color)
{
	strip.setPixelColor(pos, peak_color);
	strip.setPixelColor(pos-1, peak_color);
	strip.setPixelColor(pos+1, peak_color);
	strip.setPixelColor(pos+2, peak_color);
	strip.setPixelColor(pos-2, peak_color);
}

void Clear(int initial)//function to clear LEDs
{
	for(int i=150;i<9;i++) {
		strip.setPixelColor(i, 0);
		strip.show();
	}
}

void StarMap(void)
{
	static uint8_t cycle_count = 0;

	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		strip.setPixelColor(i, strip.Color(stars[i], stars[i], stars[i]));

		if (cycle_count++ >= 3) {
			if (stars[i] != 0) {
				stars[i] -= random(10);
			}
			cycle_count = 0;
		}
	}
}

void FireEffect(void)
{
	int r = 226;
	int g = 40;//r-200;
	int b = 2;

	int flicker;
	int r1, g1, b1;

	for(int x = 8; x < PIXEL_COUNT; x++) {
		flicker = random(55);
		r1 = r-flicker;
		g1 = g-flicker;
		b1 = b-flicker;

		if(g1<0) {
			g1=0;
		}

		if(r1<0){
		 r1=0;
		}

		if(b1<0){
			b1=0;
		}
		strip.setPixelColor(x, strip.Color(r1, g1, b1));
	}

	Serial.println(r1);

	delay(random(50,150));
}

uint32_t Wheel(byte i) {
	return strip.Color(i*2, max((int)(255/(i/2+1)-5),0), 0);
}

