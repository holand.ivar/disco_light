/*
 * font.h
 *
 * Created: 29.08.2019 09:30:22
 *  Author: M43909
 */


#ifndef FONT_H_
#define FONT_H_

#include <stdint.h>
#include "bitmapDB.h"

// Font data for Britannic Bold 8pt
extern const uint8_t Bitmaps[];
extern const FONT_INFO FontInfo;
extern const FONT_CHAR_INFO Descriptors[];


#endif /* FONT_H_ */