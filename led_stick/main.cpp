// Arduino Beat Detector By Damian Peckett 2015
// License: Public Domain.

// Our Global Sample Rate, 5000hz
#define SAMPLEPERIODUS 200

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#define PIXEL_PIN    6    // Digital IO pin connected to the NeoPixels.

#define PIXEL_COUNT 100 //30

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);


void PeakVisualizer(int pos,uint32_t peak_color);
void StarMap(float envelope);
void FireEffect(void);
uint32_t Wheel(byte i);

enum modes {
	VUMETER,
	TRIPPEL_PEAKS,
	VU_FROM_CENTER,
	ALL_LEDS,
	MODES_LAST,
	STARMAP,
	FIRE,
};

void setup();
void loop();
float bassFilter(float sample);
float envelopeFilter(float sample);
float beatFilter(float sample);

void PeakVisualizer(int pos,uint32_t peak_color);
void StarMap(void);
void FireEffect(void);
uint32_t Wheel(byte i);
void Glow(int initial, uint32_t color = 0);
void glow_from_center(int vu);

#define NUM_COLORS 5

uint32_t colors[] = {
	strip.Color(255, 0, 0),
	strip.Color(0, 0, 255),
	strip.Color(0, 255, 0),
	strip.Color(255, 255, 255),
	strip.Color(255, 128, 0),
	strip.Color(255, 0, 255),
	strip.Color(0xe6, 0x67, 0xff),
	strip.Color(0x1f, 0xff, 0xfb)
};

uint32_t peak_color;

int cycle_count = 10;
int VU_max;

enum modes mode = VUMETER;

uint8_t number_of_peaks = 0;

int8_t stars[PIXEL_COUNT];

int main (void)
{
	init();
	setup();

	while (1) {
		loop();
	}
}

void setup()
{
	// Set ADC to 77khz, max for 10bit
	sbi(ADCSRA,ADPS2);
	cbi(ADCSRA,ADPS1);
	cbi(ADCSRA,ADPS0);

	strip.begin();
	strip.show(); // Initialize all pixels to 'off'

	int seed = analogRead(0)-503.f;

	randomSeed(seed);

	//The pin with the LED
	//pinMode(2, OUTPUT);
	DDRB |= (1 << PINB5);
}

// 20 - 200hz Single Pole Bandpass IIR Filter
float bassFilter(float sample)
{
	static float xv[3] = {0,0,0}, yv[3] = {0,0,0};
	xv[0] = xv[1]; xv[1] = xv[2];
	xv[2] = (sample) / 3.f; // change here to values close to 2, to adapt for stronger or weeker sources of line level audio


	yv[0] = yv[1]; yv[1] = yv[2];
	yv[2] = (xv[2] - xv[0])
	+ (-0.7960060012f * yv[0]) + (1.7903124146f * yv[1]);
	return yv[2];
}

// 10hz Single Pole Lowpass IIR Filter
float envelopeFilter(float sample)
{ //10hz low pass
	static float xv[2] = {0,0}, yv[2] = {0,0};
	xv[0] = xv[1];
	xv[1] = sample / 50.f;
	yv[0] = yv[1];
	yv[1] = (xv[0] + xv[1]) + (0.9875119299f * yv[0]);
	return yv[1];
}

// 1.7 - 3.0hz Single Pole Bandpass IIR Filter
float beatFilter(float sample)
{
	static float xv[3] = {0,0,0}, yv[3] = {0,0,0};
	xv[0] = xv[1]; xv[1] = xv[2];
	xv[2] = sample / 2.7f;
	yv[0] = yv[1]; yv[1] = yv[2];
	yv[2] = (xv[2] - xv[0])
	+ (-0.7169861741f * yv[0]) + (1.4453653501f * yv[1]);
	return yv[2];
}

void loop()
{
	unsigned long time = micros(); // Used to track rate
	float sample, value, envelope, beat;//, thresh;
	unsigned char i;
	float amplifier = 4.2f;
	unsigned long last_bpm_check = millis();
	uint16_t bpm_count = 0;

	for(i = 0;;++i){

		if (VU_max < 5) {
			peak_color = 0;
		}

		if (!--cycle_count) {

			if (mode == TRIPPEL_PEAKS) {
				cycle_count = 75;
				VU_max -=1;
			} else {
				VU_max--;
				cycle_count = 150;
			}
		}

		// Read ADC and center so +-512
		sample = (float)analogRead(0)-503.f;

		sample *= amplifier;

		// Filter only bass component
		value = bassFilter(sample);

		// Take signal amplitude and filter
		if (value < 0){
			value = -value;
		}

		envelope = envelopeFilter(value);

		// Every 200 samples (25hz) filter the envelope
		if(i == 200) {
			// Filter out repeating bass sounds 100 - 180bpm
			beat = beatFilter(envelope);

			if (VU_max < envelope) {
				VU_max = envelope;

				if (VU_max > PIXEL_COUNT + 10) {
					VU_max = PIXEL_COUNT + 10;
				}
			}


			if (envelope > PIXEL_COUNT*0.75) {
				amplifier = amplifier - 0.1f;
			} else if (envelope < PIXEL_COUNT*0.5) {
				amplifier = amplifier + 0.1f;
			}

			if (amplifier > 6.0f) {
				amplifier = 6.0f;
			} else if (amplifier < 0.2f) {
				amplifier = 0.2f;
			}


			// Threshold it based on potentiometer on AN1
			//thresh = 0.02f * (float)analogRead(1);


			// If we are above threshold, light up LED
			if(beat > 10) {

				bpm_count++;

				peak_color = colors[random(sizeof(colors)/4)];

				if (mode == STARMAP) {
					stars[random(PIXEL_COUNT)] = random(127);
					stars[random(PIXEL_COUNT)] = random(127);
					stars[random(PIXEL_COUNT)] = random(127);
				}

// 				if (number_of_peaks++ > 100) {
// 					mode = (enum modes)random((uint8_t)MODES_LAST);
// 					number_of_peaks = 0;
// 				}


				PORTB |= (1 << PINB5);
			} else {
				PORTB &= ~(1 << PINB5);
			}

			switch (mode) {
				case VUMETER:
				Glow(envelope);
				PeakVisualizer(VU_max, peak_color);
				//Glow(PIXEL_COUNT);
				break;
				case TRIPPEL_PEAKS:
				Glow(0);
				PeakVisualizer(VU_max, peak_color);
				PeakVisualizer(VU_max*2/3, peak_color);
				PeakVisualizer(VU_max/3, peak_color);
				break;
				case ALL_LEDS:
				Glow(111, peak_color);
				break;
				case VU_FROM_CENTER:
				glow_from_center(envelope);
				break;
				case STARMAP:
				StarMap(envelope);
				break;
				case FIRE:
				FireEffect();
				break;
				default:
				Glow(0);
			}

			//mode = STARMAP;

			strip.show();

			//Reset sample counter
			i = 0;
		}

		if ((last_bpm_check + 10*1000) < millis()) {
			last_bpm_check = millis();

			if ((bpm_count) < 10) {
				mode = STARMAP;
			} else if ((bpm_count) > 40) {
				mode = (enum modes)random((uint8_t)MODES_LAST);
			} else if ((bpm_count) > 50) {
				mode = ALL_LEDS;
			}

			bpm_count = 0;
		}


		// Consume excess clock cycles, to keep at 5000 hz
		for(unsigned long up = time+SAMPLEPERIODUS; time > 20 && time < up; time = micros());
	}
}


void Glow(int initial, uint32_t color)//function to glow LEDs
{
	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (i < initial) {
			if (color == 0) {
				strip.setPixelColor(i, Wheel(i));
			} else {
				strip.setPixelColor(i, color);
			}
		} else {
			strip.setPixelColor(i, 0);
		}

	}
}

void glow_from_center(int vu)
{
	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (i < (56-vu/2) || i > (56+vu/2)) {
			strip.setPixelColor(i, 0);
		} else {
			strip.setPixelColor(i,Wheel(abs(i-56)));
		}
	}
}

void PeakVisualizer(int pos,uint32_t peak_color)
{
	strip.setPixelColor(pos-2, peak_color);
	strip.setPixelColor(pos-1, peak_color);
	strip.setPixelColor(pos, peak_color);
	strip.setPixelColor(pos+1, peak_color);
	strip.setPixelColor(pos+2, peak_color);
}

void Clear(int initial)//function to clear LEDs
{
	for(int i=150; i<9; i++) {
		strip.setPixelColor(i, 0);
		strip.show();
	}
}

void StarMap(float envelope)
{
	static uint8_t cycle_count = 0;

	uint8_t brightness = envelope/100.0 * 15 + 1;

	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		strip.setPixelColor(i, strip.Color(stars[i]*2, stars[i]*2, stars[i]*2));

		if (stars[i] == 0) {
			strip.setPixelColor(i, strip.Color(brightness, brightness, 0));
		}

		if (stars[i] > 0) {
			stars[i] -= random(2);
			brightness = 1;
		}

		if (stars[i] < 0) {
			stars[i] = 0;
		}

		if (cycle_count++ >= 3) {

			cycle_count = 0;
		}
	}
}

void FireEffect(void)
{
	int r = 226;
	int g = 40;//r-200;
	int b = 2;

	int flicker;
	int r1, g1, b1;

	for(int x = 8; x < PIXEL_COUNT; x++) {
		flicker = random(55);
		r1 = r - flicker;
		g1 = g - flicker;
		b1 = b - flicker;

		if (g1 < 0) {
			g1 = 0;
		}

		if (r1 < 0){
			r1 = 0;
		}

		if (b1 < 0){
			b1 = 0;
		}
		strip.setPixelColor(x, strip.Color(r1, g1, b1));
	}

	//Serial.println(r1);

	delay(random(50,150));
}

uint32_t Wheel(byte i)
{
	return strip.Color(i*2, max((int)(255/(i/2+1)-5),0), 0);
}
